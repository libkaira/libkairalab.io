---
title: "Linux"
layout: article
---


# Implementation

## Processing Shell-Processed Text on Linux

The shells on linux will preprocess the command line before passing it to the program. The shell
will expand environment variables, glob patterns and escape special characters. The shell will also
remove comments and remove leading and trailing whitespace. Then, we need a separate grammar to
describe the processing of the post-processed command line. One of the main issues with directly
using the input from the shell is the parsing of multivalues. Given an input argument `--option
a,b`, the linux shell will pass  the argument to the program as `["--option", "a,b"]`. However, what
we want is `["--option", "a", "b"]`. Therefore, in order to support multivalues, we need to
pre-parse the command line and then pass it to the parser.


The pre-parser will split the multivalues into separate arguments and then pass it to the parser.
The algorithm for the pre-parser is described in {{<listref "preparser.ebnf">}}. We also use the
preparser to process the escaped commas `,` in the multivalues. The preparser will replace the
escaped commas with a comma in the processed values. The preprocessor assumes that none of the keys
or the arguments will contain commas.


{{< listing lang="ebnf" label="lexer.linux.ebnf" caption="Lexer on Linux">}}
{{<highlight ebnf "linenos=table" >}}

{{</highlight>}}
{{</listing>}}


