---
title: "Home"
layout: home
toc: true
---

Kaira is a declarative command line parser for C++. With Kaira, the command line parser can be
specified in a JSON format.


# Grammar


## Tokenization


{{< listing lang="ebnf" label="grammar.ebnf" caption="Grammar">}}
{{<highlight ebnf "linenos=table" >}}
identifier = letter , { letter | digit | idsymbol } ;
key = hyphen, hyphen, identifier ;
multivalue = value, { comma, value } ;
value = valchar, { valchar } ;

valchar =  letter | digit | valsymbol | escomma ;

idsymbol =  hyphen | '.' | '_' ;

hyphen =   '-' ;

escomma = '\',comma;

comma =     ',' ;


valsymbol = '%' | '+' | '/' | ':' | '=' | '?' | '~' | '@' | '#' | '!'
          | "'" | '$' | '&' | '"' | '(' | ')' | ';' | '<' | '>' | '['
          | ']' | '^' | '`' | '{' | '|' | '}' | '\' | '*' ;

letter =    'A' | 'B' | 'C' | 'D' | 'E' | 'F' | 'G' | 'H' | 'I' | 'J'
          | 'K' | 'L' | 'M' | 'N' | 'O' | 'P' | 'Q' | 'R' | 'S' | 'T'
          | 'U' | 'V' | 'W' | 'X' | 'Y' | 'Z' | 'a' | 'b' | 'c' | 'd'
          | 'e' | 'f' | 'g' | 'h' | 'i' | 'j' | 'k' | 'l' | 'm' | 'n'
          | 'o' | 'p' | 'q' | 'r' | 's' | 't' | 'u' | 'v' | 'w' | 'x'
          | 'y' | 'z' ;

digit =     '0' | '1' | '2' | '3' | '4' | '5' | '6' | '7' | '8' | '9' ;
{{</highlight>}}
{{</listing>}}
